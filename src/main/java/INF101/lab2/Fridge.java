package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList <FridgeItem> items = new ArrayList<FridgeItem>();
    
    
    public int nItemsInFridge() {
        return items.size();
    }

    public int totalSize(){
        return 20;
    }
    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize()){
            items.add(item);
            return true;
        }
        else {
            return false;
        }
    }
    @Override
    public void takeOut(FridgeItem item) {
        if(!items.contains(item)) {
            throw new NoSuchElementException();
        }
        else {
            items.remove(item);
        }   
    }
    @Override
    public void emptyFridge() {
        items.clear();
        
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++){
            FridgeItem item = items.get(i);
            if (item.hasExpired()){
                expiredFood.add(item);
            }
        }
        for (FridgeItem expiredItem : expiredFood){
            items.remove(expiredItem);
             }
        return expiredFood;
    }


}


